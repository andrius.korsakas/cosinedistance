/**
 * Class to calculate cosine distance between documents.
 * 
 * @author Andrius Korsakas
 */
package ie.gmit.sw;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class CosineDistanceCalculator {
  private Map<String, List<Index>> db;
  private Map<String, Integer> queryFileSequence;
  private Map<String, Map<String, Integer>> subjectFileSequence;

  public CosineDistanceCalculator(Map<String, List<Index>> db) {
    this.db = db;
    subjectFileSequence = new HashMap<>();
    queryFileSequence = new HashMap<>();
  }

  /**
   * Creates shingle maps for each file.
   * Checks if file is query or subject file and calls required method.
   * 
   * @param file file name of a file to decide if it is query of subject file
   */
  public void createSequenceMaps(String file) {
    if(file.equalsIgnoreCase("queryFile")){
      db.forEach((k,v) -> populateQueryMap(k, v, file));
    }else {
      populateSujbjectMaps(file);
    }
  }
  /**
   * Method populating subject file map.
   * Takes file name as parameter.
   * For each shingle in shingle map, getting list of Indexes and adding to new map for specified file name.
   * Map with shingles and frequencies added to subject files map.
   * 
   * @param file file name of a subject file, used as a key in a subjects file map
   */
  private void populateSujbjectMaps(String file) {
    HashMap<String, Integer> subject = new HashMap<>();
    for(String s : db.keySet()) {
      List<Index> l = new ArrayList<>(db.get(s));
      for(Index i : l) {
        if(i.getFileName().equalsIgnoreCase(file)) {
          subject.put(s, i.getFrequency());
        }
      }
    }
    subjectFileSequence.put(file, subject);
  }
  /**
   * Creates query file map.
   * 
   * @param shingle shingle of a query file
   * @param list list of Indexes containing file names and frequencies of a shingle
   * @param file query file name to filter List of indexes for query file information
   */
  private void populateQueryMap(String shingle, List<Index> list, String file) {
    List<Index> l = new ArrayList<>(list);
    for(Index i : l) {
      if(i.getFileName().equalsIgnoreCase(file)) {
        queryFileSequence.put(shingle, i.getFrequency());
      }
    } 
  }
  /**
   * Gets unique shingles from query and subject maps.
   * Create HashSet of subject file shingles.
   * Using retainAll method getting shingles common to both query and subject files.
   * 
   * Returns shingles contained by subject and query maps.
   * @param subject map containing shingles as keys
   * @return intersection between query and single subject file
   */
  private HashSet<String> getUniqueShingles(Map<String, Integer> subject){
    HashSet<String> intersection = new HashSet<>(subject.keySet());
    intersection.retainAll(queryFileSequence.keySet());
    return intersection;
  }
  /**
   * Calculates dot product of query and subject file map.
   * 
   * @param intersection of query and subject map shingles 
   * @param subject map of keys - shingles and values - frequencies
   * @return dot product of query and suject files
   */
  private double dotProduct(HashSet<String> intersection, Map<String, Integer> subject) {
    double dotProduct = 0;
    for(String shingle : intersection) {
      dotProduct += queryFileSequence.get(shingle) * subject.get(shingle);
    }
    return dotProduct;
  }
  /**
   * Calculates magnitude of a shingle sequence.
   * 
   * @param item an item in a shingle sequence
   * @return magnitude of a sequence
   */
  private double magnitude(Map<String, Integer> item) {
    double magnitude = 0;
    for(String s : item.keySet()) {
      magnitude += Math.pow(item.get(s), 2);
    }
    return magnitude;
  }
  /**
   * Calculates cosine distance between query and subject files and outputs it to screen.
   * For each loop to calculation of each file in subject map against query file map.
   * Add results to result map and output to screen.
   * 
   */
  public void cosineSimilarity() {
    Map<String, Double> cosSimilarity = new TreeMap<>();
    HashSet<String> intersection;
    double dotProduct = 0;
    double magnitude = 0;
    double queryFileMagnitude = magnitude(queryFileSequence);

    for(String s : subjectFileSequence.keySet()) {   
      intersection = getUniqueShingles(subjectFileSequence.get(s));
      dotProduct = dotProduct(intersection, subjectFileSequence.get(s));
      magnitude = magnitude(subjectFileSequence.get(s));     
      double cosSim = (dotProduct / Math.sqrt(queryFileMagnitude * magnitude));
      cosSim *= 100;

      cosSimilarity.put(s, cosSim);
    }
    cosSimilarity.forEach((k,v) -> System.out.printf("File: " + k + " Similarity: %.2f%%\n" , v));
  }

}
