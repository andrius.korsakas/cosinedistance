/**
 * Class to process shingles from queue and add them to map database.
 * Implements Callable interface.
 * 
 * @author Andrius Korsakas
 */
package ie.gmit.sw;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;

public class ShingleProcessor implements Callable<String> {
  private BlockingQueue<Word> q;
  private Map<String, List<Index>> map;
  private boolean running = true;
  private String fileName = null;

  public ShingleProcessor(BlockingQueue<Word> q, Map<String, List<Index>> map) {
    this.q = q;
    this.map = map;
  }
  
  /**
   * Callable call() method.
   * Takes elements from queue and adds them to shingle map.
   * There is a slight variation due to unsynchronized access to List of Index
   * When instance of Poison class received, returns file name indicating that file been finished processing. 
   * 
   * @return fileName file name of a file which produced Poison instance
   */
  @Override
  public String call() throws Exception {
    while(running) {

      Word w = null;

      try {
        w = q.take();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      if(w instanceof Poison) {
        fileName = w.getFileName();
        running = false;
      }else {
        String shingle = w.getShingle();
        List<Index> list = null;
        if(!map.containsKey(shingle)) {
          list = new ArrayList<>();
          list.add(new Index(1, w.getFileName()));
          map.put(shingle, list);
        }else {
          list = new ArrayList<>(map.get(shingle));
          boolean newFile = true;
          for(Index i : list) {
            if(i.getFileName().equalsIgnoreCase(w.getFileName())) {
              newFile = false;
              i.setFrequency(i.getFrequency() + 1);
            }
          }
          if(newFile) {
            list.add(new Index(1, w.getFileName()));
          }
          map.replace(shingle, list);
        }
      }

      if(!running) {
        System.out.println(fileName + " finished.");
      }
    }
    return fileName;
  }

}
