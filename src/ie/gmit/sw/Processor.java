/**
 * Processor class to process query and subject files.
 * Creates thread for each files and passes FileParser object to it.
 * 
 */
package ie.gmit.sw;

import java.io.File;
import java.util.concurrent.BlockingQueue;

public class Processor {
  private BlockingQueue<Word> queue;
  
  public Processor(BlockingQueue<Word> queue) {
    this.queue = queue;
  }
  /**
   * Creates and starts threads for query and subject files.
   * Identifying query thread by givint it name.
   * 
   * @param queryFile
   * @param subjectFiles
   * @param fileCount
   */
  public void processFiles(File queryFile, File[] subjectFiles, int fileCount) {
    
    new Thread(new FileParser(queue, queryFile), "queryThread").start();

    for(File f : subjectFiles) {
      new Thread(new FileParser(queue, f)).start();
    }
  }
}
