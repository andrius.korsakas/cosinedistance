/**
 * Main menu for CosineDistance application.
 * Contains prompts for query and subject files.
 * 
 * @author Andrius Korsakas
 */
package ie.gmit.sw;

import java.util.Scanner;

public final class Menu {
  private Scanner in;
  private boolean running = true;
  private CosineDistance cosineDistance;
  private String input;
  String defaultDir = "/home/ak/Documents/college/OO/";
  String defaultQuery = "/home/ak/Documents/college/WarAn";

  public Menu() {
    in = new Scanner(System.in);
    running = true;
    cosineDistance = new CosineDistance();
  }
  
/**
 * Method after calling executing main menu in do while loop.
 * User enters query and subject file locations.
 */
  public void execute() {
    System.out.println(welcomeMessage());
    do {
      cosineDistance.setSubjectFileDir(subjectDir());
      cosineDistance.setQueryFile(queryFile());
      cosineDistance.processFiles();
      exit();     
    } while (running);
  }

  private String welcomeMessage() {
    return "*****Document Comparison Service*****";
  }
  
  private String queryFile() {
    System.out.println("Enter query file: ");
    return in.next();
  }
  
  private String subjectDir() {
    System.out.print("Enter Subject Directory > ");
    return in.next();
  }
  
  private void exit() {
    System.out.println("Type exit to quit or c to continue.");
    this.input = in.next();
    if(input.equalsIgnoreCase("exit")) {
      this.running = false;
    }
  }

}
