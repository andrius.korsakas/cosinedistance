/**
 * A main control class of CosineDistance application.
 *
 *@author Andrius Korsakas
 */
package ie.gmit.sw;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CosineDistance {
  private Processor processor;
  private String subjectFileDir;
  private String queryFile;
  private Map<String, List<Index>> db = new ConcurrentHashMap<>();
  private BlockingQueue<Word> queue = new ArrayBlockingQueue<>(50000);
  private int fileCount;
  private CosineDistanceCalculator cosCalculator;
  
  public CosineDistance() {
    processor = new Processor(this.queue);
    cosCalculator = new CosineDistanceCalculator(db);
  }
  
  /**
   * Method for processing files.
   * No parameters or arguments.
   * Gets list of files.
   * Initializes file count.
   * 
   * Calls Processor class method processFiles.
   * Then calls processShingles method.
   * 
   */
  public void processFiles() {
    File[] files = new File(getSubjectFileDir()).listFiles(new FilenameFilter() {
      public boolean accept(File current, String name) {
        return new File(current, name).isFile();
      }
    });
    
    File queryFile = new File(getQueryFile());
    
    this.fileCount = files.length + 1;
    
    processor.processFiles(queryFile, files, fileCount);
    
    processShingles();
  }
  
  /**
   * Creates ExecutorService pool thread for ShingleProcessor.
   * List ShingleProcesseror returned future.
   * For each filename calling CosineDistanceCalculator method to populate maps.
   * Waiting until pool is terminated indicating that shingle processing finished.
   * Calling method to calculate similarity.
   * 
   */
  private void processShingles() {
   
    ExecutorService pool = Executors.newFixedThreadPool(this.fileCount);
    
    List<Future<String>> fileList = new ArrayList<>();
    
    for(int i = 0; i < fileCount; i++) {
      Future<String> file = pool.submit(new ShingleProcessor(queue, db));
      fileList.add(file);
    }
    
    for(Future<String> s : fileList) {
      try {
        cosCalculator.createSequenceMaps(s.get());
      } catch (InterruptedException e) {
        e.printStackTrace();
      } catch (ExecutionException e) {
        e.printStackTrace();
      }
    }
    
    pool.shutdown();
    
    while(!pool.isTerminated());
    
    calculateSimilarity();
  }
  /**
   * Calls CosindeDistanceCalculator method to calculate similarities between files.
   */
  private void calculateSimilarity() {
    cosCalculator.cosineSimilarity();
  }

  public String getQueryFile() {
    return queryFile;
  }

  public void setQueryFile(String queryFile) {
    this.queryFile = queryFile;
  }

  public String getSubjectFileDir() {
    return subjectFileDir;
  }

  public void setSubjectFileDir(String subjectFileDir) {
    this.subjectFileDir = subjectFileDir;
  }
}
