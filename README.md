# CosineDistance

Calculate cosine distance between query file and multiple subject files.<br>
Main focus on threading of Java.<br>
Runnable, Callable tasks, Future, Thread pools,  Executors, Data Structures, Extensive JavaDocs...<br>



Main Features 

Calculate cosine distance between query file and multiple subject files.

Query and subject files are each processed on a separate thread using Processor class to create threads and pass FileParser objects to thread. FileParser implements Runnable interface and runs on a separate thread processing each file. Using an algorithm, each line of file is splitted into three word shingles and added to BlockingQueue. 

ShinleProcessor implements Callable interface and returns file name after taking Poison instance from queue. In CosineDistance class, ExecutorService is created with a total file count thread pool size. For each file, there is a ShingleProcessor thread running, but working on the queue as a whole. When taking elements from queue, elemenet is added to ConcurrentSkipListMap which contains all shingles from all files. ShingleProcessor returns file name of file which added Poison object.

Making a List of Future<String> from ShingleProcessor returned String. 

For each String in List<Future<String>, calling CosineDistanceCalculator method. Passing String(representing file name) as a parameter. Query file shingle and frequency map is created if file is query file. If not, creating separate map Map<String, Map<String, Integer>>. Reperesenting file name as a key and shingle and frequency as a value. 

When ExecutorService pool thread is terminated, it indicates that all ShingleProcess callable threads returned and queue is empty. Calling CosineDistanceCalculator method to calculate cosine similarity between query and subject files. 
